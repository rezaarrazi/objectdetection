# ObjectDetection
## Label Pictures
Download labelImg

[LabelImg GitHub link](https://github.com/tzutalin/labelImg)

[LabelImg download link](https://www.dropbox.com/s/tq7zfrcwl44vxan/windows_v1.6.0.zip?dl=1)

Download and install LabelImg, point it to your image directory, and then draw a box around each object in each image. Repeat the process for all the images in the \Image\source directory.

Set save directory to Data\Anotation. LabelImg saves a `.xml` file containing the label data for each image. These `.xml` files will be used to generate TFRecords, which are one of the inputs to the TensorFlow trainer. Once you have labeled and saved each image, there will be one `.xml` file for each image in the \Anotation.

## Generate Training Data

With the images labeled, it’s time to generate the TFRecords that serve as input data to the TensorFlow training model. This step uses the `xml_to_csv.py` and `generate_tfrecord.py` scripts

First, the image `.xml` data will be used to create `.csv` files containing all the data. From the \objectdetection folder, issue the following command in the Anaconda command prompt:
```
(tf_gpu) D:\Reza\Dokumen\Ichiro\DeepLearning\objectdetection> python xml_to_csv.py
```
This creates a objectdetection_labels.csv file in the \objectdetection\Data folder.

Next, we need to split dataset into train and test data. This step uses `split labels.ipynb`. Open the jupyter notebook and run the script. It will creates `train_label.csv` and `test_label.csv` in the \objectdetection\Data folder.

Then, generate the TFRecord files by issuing these commands from the \object_detection folder:
```
python generate_tfrecord.py --csv_input=Data\train_labels.csv --image_dir=Data\Image\Source --output_path=Data\train.record
python generate_tfrecord.py --csv_input=Data\test_labels.csv --image_dir=Data\Image\Source --output_path=Data\test.record
```
These generate a train.record and a test.record file in \objectdetection\Data. These will be used to train the new object detection classifier.

## Create Label Map and Configure Training
The last thing to do before training is to create a label map and edit the training configuration file.

#### Label map
The label map tells the trainer what each object is by defining a mapping of class names to class ID numbers. Use a text editor to create a new file and save it as `object-detection.pbtxt` in the D:\Reza\Dokumen\Ichiro\DeepLearning\objectdetection\training folder. (Make sure the file type is .pbtxt, not .txt !) In the text editor, copy or type in the label map in the format below:
```
item {
  id: 1
  name: 'bola'
}

item {
  id: 2
  name: 'gawang'
}

item {
  id: 3
  name: 'L_junction'
}

item {
  id: 4
  name: 'T_junction'
}

item {
  id: 5
  name: 'X_junction'
}

item {
  id: 6
  name: 'penalty_point'
}
```

## Run the Training
*As of version 1.9, TensorFlow has deprecated the `train.py` file and replaced it with `model_main.py` file. Fortunately, the `train.py` file is still available in the /object_detection/legacy folder.

Here we go! From the script directory, issue the following command to begin training:
```
python train.py --logtostderr --train_dir=..\..\..\..\..\objectdetection\training\ --pipeline_config_path=..\..\..\..\..\objectdetection\training\ssd_mobilenet_v1.config
```

You can view the progress of the training job by using TensorBoard. To do this, open a new instance of Anaconda Prompt, activate the tf_gpu virtual environment, change to the D:\Reza\Dokumen\Ichiro\DeepLearning\tensorflow\models\research\object_detection\legacy directory, and issue the following command:
```
(tf_gpu) D:\Reza\Dokumen\Ichiro\DeepLearning\tensorflow\models\research\object_detection\legacy>tensorboard --logdir=training
```
This will create a webpage on your local machine at MSI:6006, which can be viewed through a web browser. The TensorBoard page provides information and graphs that show how the training is progressing. One important graph is the Loss graph, which shows the overall loss of the classifier over time.

## Export Inference Graph
Now that training is complete, the last step is to generate the frozen inference graph (.pb file). From the \objectdetection folder, issue the following command, where `XXXX` in `model.ckpt-XXXX` should be replaced with the highest-numbered `.ckpt` file in the training folder:
```
python export_inference_graph.py --input_type image_tensor --pipeline_config_path ..\..\..\..\objectdetection\training\ssd_mobilenet_v1.config --trained_checkpoint_prefix ..\..\..\..\objectdetection\training\model.ckpt-3174 --output_directory ..\..\..\..\objectdetection\training\inference_graph
```
This creates a `frozen_inference_graph.pb` file in the \objectdetection\training\inference_graph folder. The `.pb` file contains the object detection classifier.

## Use Your Newly Trained Object Detection Classifier!
The object detection classifier is all ready to go! Use `tes.py` to test it out on an video, or webcam feed.